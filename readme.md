**Godot Dungeon Generator**

A very simple way to randomly generate dungeons. Essentially follows the same logic as shown [here](http://gamedevelopment.tutsplus.com/tutorials/create-a-procedurally-generated-dungeon-cave-system--gamedev-10099).

Basically the code works like this:
1. Fill the world with tiles.
2. Add rooms to an array, discarding any that overlap. Make a variable amount of attempts to add rooms.
3. For each room in the array, carve it out of the map.
4. For each room in the array, carve a horizontal corridor leading from the centre of the room, to the centre of the next room.
5. While still looping, carve a vertical corridor from the end of the horizontal corridor to the centre of the next room in the array.
6. Finally, run neighbour aware function over every tile to make sure the correct one is displayed. Eventually will also include a 'floor' tile for empty tiles.

Tiles are by [Kenney](https://www.kenney.nl/), under a [Creative Commons 0 - Public Domain](https://creativecommons.org/publicdomain/zero/1.0/) licence.

Code is public domain, but if you do decide to use it for anything, feel free to show me as I'd love to see it!